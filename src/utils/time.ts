export default function delay(time: number): void {
  const startTime = Date.now();
  while (Date.now() - startTime < time) {}
}
