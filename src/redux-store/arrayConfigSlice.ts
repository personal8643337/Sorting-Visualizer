import { createSlice } from '@reduxjs/toolkit';
import SortingAlgos from '../const/sorting';

export const arrayConfigSlice = createSlice({
  name: 'arrayConfig',
  initialState: {
    delay: 5,
    size: 1000,
    mode: SortingAlgos.bubble,
  },
  reducers: {
    changeMode: (state, action) => {
      state.mode = action.payload;
    },
    changeSize: (state, action) => {
      state.size = action.payload;
    },
    changeDelay: (state, action) => {
      state.delay = action.payload;
    },
  },
});

export const { changeMode, changeSize, changeDelay } = arrayConfigSlice.actions;

export default arrayConfigSlice.reducer;
