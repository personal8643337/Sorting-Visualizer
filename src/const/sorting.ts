const ALGORITHMS = {
  bubble: 'BUBBLE',
  selection: 'SELECTION',
  insertion: 'INSERTION',
  merge: 'MERGE',
  heap: 'HEAP',
  radix: 'RADIX',
  quick: 'QUICK',
  bitonic: 'BITONIC',
};

export default ALGORITHMS;
