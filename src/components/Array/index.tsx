import { useEffect, useState } from 'react';
import styles from './styles.module.scss';
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from '../../redux-store';
import startSorting from '../../sorting-logic/logic';
import { toggleIsSorting } from '../../redux-store/sortingSlice';
import { quickSortHelper } from '../../sorting-logic/logic';

const createRandomArray = (size: number) => {
  const newArr = Array(size);

  for (let i = 1; i <= size; i++) {
    newArr[i] = i;
  }

  // shuffle array randomly
  newArr.sort((a, b) => 0.5 - Math.random());
  return newArr;
};

const SortingVisualizer = () => {
  const size = useSelector((state: RootState) => state.config.size);
  const mode = useSelector((state: RootState) => state.config.mode);
  const isSorting = useSelector((state: RootState) => state.sorting.isSorting);
  const [arr, setArr] = useState<number[]>([]);
  const dispatch = useDispatch();

  useEffect(() => {
    const randomArr = createRandomArray(size);
    setArr(randomArr);
  }, [size]);

  useEffect(() => {
    const sortedArr = [...arr].sort((a, b) => a - b);

    if (isSorting) {
      if (JSON.stringify(arr) === JSON.stringify(sortedArr)) {
        if (!!quickSortHelper.length) {
          quickSortHelper.splice(0, arr.length);
        }
        dispatch(toggleIsSorting());
        return;
      }
      const newArr = [...arr];
      startSorting(setArr, newArr, mode);
    }
  }, [isSorting, arr, dispatch, mode]);

  return (
    <div className={styles.container}>
      {arr.map((item, index) => (
        <div
          key={index}
          className={styles.sortItem}
          style={{
            height: `${(item * 750) / size}px`,
            width: `${1800 / size}%`,
          }}
        ></div>
      ))}
    </div>
  );
};

export default SortingVisualizer;
