import styles from './styles.module.scss';
import ALGORITHMS from '../../const/sorting';
import {
  changeMode,
  changeDelay,
  changeSize,
} from '../../redux-store/arrayConfigSlice';
import { toggleIsSorting } from '../../redux-store/sortingSlice';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../redux-store';

const Header = () => {
  const mode = useSelector((state: RootState) => state.config.mode);
  const arrSize = useSelector((state: RootState) => state.config.size);
  const delay = useSelector((state: RootState) => state.config.delay);
  const isSorting = useSelector((state: RootState) => state.sorting.isSorting);
  const dispatch = useDispatch();

  const modeChangeHandler = (event: any) => {
    dispatch(changeMode(event.target.value));
  };
  const submitHandler = (event: any) => {
    dispatch(toggleIsSorting());
    event.preventDefault();
  };

  const changeArraySize = (event: any) => {
    dispatch(changeSize(event.target.value));
  };

  const changeDelayBetweenIteration = (event: any) => {
    dispatch(changeDelay(event.target.value));
  };

  return (
    <>
      <header className={styles.header}>
        <form onSubmit={submitHandler}>
          <div className={styles.mode}>
            <label htmlFor='modes'>Mode:</label>
            <select
              name='modes'
              id='modes'
              value={mode}
              onChange={modeChangeHandler}
              disabled={isSorting}
            >
              {Object.entries(ALGORITHMS).map(([key, value], _index) => (
                <option key={key} value={value}>
                  {key}
                </option>
              ))}
            </select>
          </div>

          <div className={styles.range}>
            <p>Size:</p>
            <input
              type='range'
              id='vol'
              name='vol'
              min='100'
              max='1000'
              value={arrSize}
              onChange={changeArraySize}
              disabled={isSorting}
            />
            <p>{arrSize}</p>
          </div>

          <div className={styles.delay}>
            <p>Delay:</p>
            <input
              type='range'
              id='delay'
              name='delay'
              min='1'
              max='20'
              value={delay}
              onChange={changeDelayBetweenIteration}
              disabled={isSorting}
            />
            <p>{delay}</p>
          </div>

          <button type='submit' disabled={isSorting}>
            Sort
          </button>
        </form>
      </header>
    </>
  );
};

export default Header;
