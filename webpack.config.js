const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  module: {
    rules: [
      {
        test: /\.html$/i,
        loader: 'extract-loader html-loader',
        exclude: /node_modules/,
        include: /src\/markdown/,
      },
    ],
    plugins: [new HtmlWebpackPlugin()],
  },
};
